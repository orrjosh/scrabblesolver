import time
import sys
from itertools import permutations,combinations

fullListFile=open("SOWPODS_complete.txt")
legalWords=[]
racks=[]
vowels=['A','E','I','O','U','Y',]
wildcard="_"
vowels=set(vowels)
onePointLetters=['A', 'E', 'I', 'O', 'U', 'L', 'N', 'R', 'S', 'T']
twoPointLetters=['D', 'G']
threePointLetters=[ 'B', 'C', 'M', 'P' ]
fourPointLetters=[ 'F', 'H', 'V', 'W', 'Y']
fivePointLetters=['K']
eightPointLetters=['J','X']
tenPointLetters=['Q','Z']
score = { "A": 1, "C": 3, "B": 3, "E": 1, "D": 2, "G": 2,
          "F": 4, "I": 1, "H": 4, "K": 5, "J": 8, "M": 3,
          "L": 1, "O": 1, "N": 1, "Q": 10, "P": 3, "S": 1,
          "R": 1, "U": 1, "T": 1, "W": 4, "V": 4, "Y": 4,
          "X": 8, "Z": 10}
testCount=0


def tryAll(rack):
    global testCount
    possibleWords=list(permutations(rack))
    validWords=[]
    for pw in possibleWords:
        testCount+=1
        testVal="".join(pw)
        if testVal in legalWords:
            validWords.append(testVal)
    return validWords

def scoreWord(word):
    wordScore=0
    for l in word:
        wordScore+=score.get(l)
    return wordScore
def doMove(rack):
    global testCount
    #removing wildcards.  they will help strategically in the future
    rack[:]=(value for value in rack if value != wildcard)
    vowelsInRack=vowels.intersection(set(rack))
    if len(vowelsInRack)==0:
        return;
    #bingo try
    validWords=tryAll(rack)
    if len(validWords)>0:
        return validWords
    #remove mid point letter. decrease difficulty of finding word while still
    #preserving high score
    rackLength=len(rack)
    for l in rack:
        if l in fourPointLetters or l in fivePointLetters:
            rack.remove(l)
            break
    if len(rack) == rackLength:
        pass
    else:
        validWords=tryAll(rack)
        if len(validWords)>0:
            return validWords
    #remove a high point letter
    rackLength=len(rack)
    for l in rack:
        if l in eightPointLetters or l in tenPointLetters:
            rack.remove(l)
            break

    if len(rack) == rackLength:
        pass
    else:
        validWords=tryAll(rack)
        if len(validWords)>0:
            return validWords
    #brute force time
    validWords=[]
    for num in range(2, len(rack)):
        possibleWords=list(combinations(rack,num))
        for pw in possibleWords:
            testCount+=1
            testVal="".join(pw)
            if testVal in legalWords:
                validWords.append(testVal)

    return validWords
def reportOutput(words):
    if words:
        scoredWords=[]
        for word in words:
            scoredWords.append((word,scoreWord(word)*2))
    else:
        print "PASS"
        return
    initialWord=max(scoredWords,key=lambda x: x[1])
    print "resolution test count:",testCount
    print "Time since start of program:",time.time()-startTime
    if showBoard=='N':
        print "Initial Word:", initialWord[0] , "Score:", initialWord[1]
    else:
        import simplescrabbleboard as ssb
        iwordList=list(initialWord[0])
        for l in range(0,len(iwordList)):
            ssb.setCell(iwordList[l],l+6,6)
        ssb.paint()
        print "Score:",initialWord[1]
##########setup shop#############
startTime=time.time()

for line in fullListFile:
    legalWords.append(line.strip())
legalWords=set(legalWords)


endSetupTime=time.time()
print "Setup time:",endSetupTime-startTime
argCount=len(sys.argv)
if argCount == 2:
	rackFile=sys.argv[1]
	showBoard=raw_input("Do you want to see board? (Y or N): ")
	showBoard=showBoard.upper()
elif argCount == 3:
	rackFile=sys.argv[1]
	showBoard=sys.argv[2].upper()
else:
	rackFile=raw_input("Enter rack file name: ")
	showBoard=raw_input("Do you want to see board? (Y or N): ")
	showBoard=showBoard.upper()

rackFile=open(rackFile)
for rack in rackFile:
	testCount=0
	rack=list(rack)
	words=doMove(rack)
	reportOutput(words)

