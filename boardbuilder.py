emptyContentRow=list(" " for x in range(0,15))
contentBoard=list(list(emptyContentRow) for x in range(0,15))
isDirty=False

def makeHorizontal():
    horizontal="|"
    for i in range(0,15,1):
        horizontal=horizontal+" - |"

    return horizontal
def makeContentRow(content):
    if len(content) != 15:
        print "Invalid content, len=",len(content)
    row="|"
    for column in content:
        row=row + " "+column+" |"
    return row

def setCell(letter, x,y):
    global contentBoard,isDirty
    contentBoard[x][y]=letter
    isDirty=True 

def paint():
    horizontal=makeHorizontal()
    for row in contentBoard:
        print horizontal
        print makeContentRow(row)
    print horizontal

paint()
setCell('r',1,1)
print "setting cell"
if isDirty:
    paint()
